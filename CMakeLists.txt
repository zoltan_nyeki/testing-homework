cmake_minimum_required(VERSION 3.16)
project(Testing)

set(CMAKE_CXX_STANDARD 17)

if (NOT CMAKE_PREFIX_PATH)
	message(WARNING "CMAKE_PREFIX_PATH is not defined, you may need to set it "
		"(-DCMAKE_PREFIX_PATH=\"path/to/Qt/lib/cmake\" or -DCMAKE_PREFIX_PATH=/usr/include/{host}/qt{version}/ on Ubuntu)")
endif ()

find_package(Qt5 COMPONENTS Core REQUIRED)

add_subdirectory(3rdparty)
add_subdirectory(src)

# enable CTest module
include(CTest)
enable_testing()

add_subdirectory(test)
