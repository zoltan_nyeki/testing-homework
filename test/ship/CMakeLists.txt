add_subdirectory(cargo)

add_executable(test-ship tst_ship.cpp)
target_link_libraries(test-ship
	PUBLIC test-main
	PUBLIC Catch2
	PUBLIC Qt5::Core
	PUBLIC ship
	PUBLIC cargo
)
add_test(NAME ShipTest COMMAND test-ship)

