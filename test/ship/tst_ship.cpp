#include <catch.hpp>

#include <ship.h>

#include <cargo-area.h>

#include "mock_cargo.h"

// Helper functions for Catch2 and Qt, they help display formatted error messages
inline std::ostream &operator<<(std::ostream &os, const QByteArray &value)
{
	return os << '"' << (value.isEmpty() ? "" : value.constData()) << '"';
}
inline std::ostream &operator<<(std::ostream &os, const QLatin1String &value)
{
	return os << '"' << value.latin1() << '"';
}
inline std::ostream &operator<<(std::ostream &os, const QString &value)
{
	return os << value.toLocal8Bit();
}

TEST_CASE("Invalid ship creation", "[ship]")
{
	SECTION("Negativ min crew")
	{
		REQUIRE_THROWS_AS(Ship(-1, 0), std::invalid_argument);
	}
	SECTION("Negativ max crew")
	{
		REQUIRE_THROWS_AS(Ship(0, -1), std::invalid_argument);
	}
	SECTION("Negativ min value")
	{
		REQUIRE_THROWS_AS(Ship(0, 10, nullptr, -1.0), std::invalid_argument);
	}
	SECTION("Max crew less then min crew")
	{
		REQUIRE_THROWS_AS(Ship(10, 5), std::invalid_argument);
	}
}

TEST_CASE("Crew", "[ship crew]")
{
	Ship ship(3, 4);

	SECTION("Hire and lay off")
	{
		SECTION("Captian", "Hire and lay off the captain")
		{
			SECTION("hire")
			{
				REQUIRE_FALSE(ship.hireCaptain(""));
				REQUIRE(ship.hireCaptain("Aladár"));
				REQUIRE_FALSE(ship.hireCaptain("Aladár"));
			}

			SECTION("layOff")
			{
				REQUIRE_FALSE(ship.layOffCaptain());
				REQUIRE(ship.hireCaptain("Aladár"));
				REQUIRE(ship.layOffCaptain());
			}
		}

		SECTION("Officer", "Hire and lay off the officer")
		{
			SECTION("hire")
			{
				REQUIRE_FALSE(ship.hireOfficer(""));
				REQUIRE(ship.hireOfficer("Aladár"));
				REQUIRE_FALSE(ship.hireOfficer("Aladár"));
			}

			SECTION("layOff")
			{
				REQUIRE_FALSE(ship.layOffOfficer());
				REQUIRE(ship.hireOfficer("Aladár"));
				REQUIRE(ship.layOffOfficer());
			}
		}

		SECTION("Crewman", "Hire and lay off the crewmen")
		{
			SECTION("hire")
			{
				REQUIRE_FALSE(ship.hireCrew(""));
				REQUIRE(ship.hireCrew("Crewman 1"));
				REQUIRE(ship.hireCrew("Crewman 2"));
				REQUIRE(ship.hireCrew("Crewman 3"));
				REQUIRE_FALSE(ship.hireCrew("Crewman 2"));
				REQUIRE(ship.hireCrew("Crewman 4"));
				REQUIRE_FALSE(ship.hireCrew("Crewman 5"));
			}

			SECTION("layOff")
			{
				REQUIRE_FALSE(ship.layOffCrew(""));
				REQUIRE(ship.hireCrew("Crewman 1"));
				REQUIRE(ship.layOffCrew("Crewman 1"));
				REQUIRE_FALSE(ship.layOffCrew("Crewman 1"));
			}
		}
	}

	SECTION("Get crew")
	{
		SECTION("Get only the crew", "captain and officer included")
		{
			SECTION("no crewman")
			{
				REQUIRE(ship.getCrew() == QString("Crew: "));
			}

			SECTION("3 crewman")
			{
				REQUIRE(ship.hireCrew("Crewman 1"));
				REQUIRE(ship.hireCrew("Crewman 2"));
				REQUIRE(ship.hireCrew("Crewman 3"));
				REQUIRE(ship.getCrew() == QString("Crew: Crewman 1, Crewman 2, Crewman 3"));
			}

			SECTION("3 crewman in sorted list")
			{
				REQUIRE(ship.hireCrew("Crewman 2"));
				REQUIRE(ship.hireCrew("Crewman 1"));
				REQUIRE(ship.hireCrew("Crewman 3"));
				REQUIRE(ship.getCrew() == QString("Crew: Crewman 1, Crewman 2, Crewman 3"));
			}
		}

		SECTION("Get full crew", "captain and officer included")
		{
			SECTION("no crewman, captain and officer")
			{
				REQUIRE(ship.getCrew(true) == QString("Captain: \nOfficer: \nCrew: "));
			}

			SECTION("3 crewman")
			{
				CHECK(ship.hireCrew("Crewman 1"));
				CHECK(ship.hireCrew("Crewman 2"));
				CHECK(ship.hireCrew("Crewman 3"));
				REQUIRE(ship.getCrew(false) == QString("Crew: Crewman 1, Crewman 2, Crewman 3"));

				SECTION("With Captain")
				{
					CHECK(ship.hireCaptain("Jack Sparrow"));
					REQUIRE(ship.getCrew(true) == QString("Captain: Jack Sparrow\nOfficer: \nCrew: Crewman 1, Crewman 2, Crewman 3"));
				}

				SECTION("With Officer")
				{
					CHECK(ship.hireOfficer("James Cook"));
					REQUIRE(ship.getCrew(true) == QString("Captain: \nOfficer: James Cook\nCrew: Crewman 1, Crewman 2, Crewman 3"));
				}

				SECTION("With Captain and officer")
				{
					REQUIRE(ship.hireCaptain("Jack Sparrow"));
					REQUIRE(ship.hireOfficer("James Cook"));
					REQUIRE(ship.getCrew(true) == QString("Captain: Jack Sparrow\nOfficer: James Cook\nCrew: Crewman 1, Crewman 2, Crewman 3"));
				}
			}
		}
	}
}

TEST_CASE("Add container", "[ship container]")
{
	SECTION("no cargo-area")
	{
		Ship ship(3, 5, nullptr);
		REQUIRE_FALSE(ship.addContainer(new cargo::Container(1.0, 1.0, 1.0)));
	}

	SECTION("there is cargo-area")
	{
		auto* cargoArea = new cargo::CargoArea(5.0, 5.0);
		Ship ship(3, 5, cargoArea);

		SECTION("nullptr container")
		{
			REQUIRE_FALSE(ship.addContainer(nullptr));
		}

		SECTION("add container")
		{
			REQUIRE(ship.addContainer(new cargo::Container(1.0, 1.0, 1.0)));
		}

		SECTION("to much weight of container")
		{
			REQUIRE(ship.addContainer(new cargo::Container(1.0, 4.4, 1.0)));
			REQUIRE(ship.addContainer(new cargo::Container(1.0, 0.4, 1.0)));
			REQUIRE_FALSE(ship.addContainer(new cargo::Container(1.0, 1.5, 1.0)));
		}

		SECTION("to much TEU of container")
		{
			REQUIRE(ship.addContainer(new cargo::Container(4.4, 1.0, 1.0)));
			REQUIRE(ship.addContainer(new cargo::Container(0.4, 1.0, 1.0)));
			REQUIRE_FALSE(ship.addContainer(new cargo::Container(0.4, 1.0, 1.0)));
		}

		SECTION("TEU is negative")
		{
			REQUIRE_FALSE(ship.addContainer(new cargo::Container(-1.0, 1.0, 1.0)));
		}
		SECTION("TEU is zero")
		{
			REQUIRE_FALSE(ship.addContainer(new cargo::Container(0.0, 1.0, 1.0)));
		}
		SECTION("Weight is negative")
		{
			REQUIRE_FALSE(ship.addContainer(new cargo::Container(1.0, -1.0, 1.0)));
		}
		SECTION("Weight is zero")
		{
			REQUIRE_FALSE(ship.addContainer(new cargo::Container(1.0, 0.0, 1.0)));
		}
		SECTION("Value is negative")
		{
			REQUIRE_FALSE(ship.addContainer(new cargo::Container(1.0, 1.0, -1.0)));
		}
		SECTION("Value is zero")
		{
			REQUIRE_FALSE(ship.addContainer(new cargo::Container(1.0, 1.0, 0.0)));
		}


	}
}

TEST_CASE("Set sail", "[ship]")
{
	auto* cargoArea = new cargo::CargoArea(5.0, 5.0);

	SECTION("No minimal cargo value")
	{
		Ship ship(3, 5, cargoArea);

		SECTION("There is no captain")
		{
			CHECK(ship.hireOfficer("James Cook"));
			CHECK(ship.hireCrew("Crewman 1"));
			CHECK(ship.hireCrew("Crewman 2"));
			CHECK(ship.hireCrew("Crewman 3"));
			REQUIRE_FALSE(ship.setSail());
		}

		SECTION("There is no officer")
		{
			CHECK(ship.hireCaptain("Jack Sparrow"));
			CHECK(ship.hireCrew("Crewman 1"));
			CHECK(ship.hireCrew("Crewman 2"));
			CHECK(ship.hireCrew("Crewman 3"));
			REQUIRE_FALSE(ship.setSail());
		}

		SECTION("There is no enough crew")
		{
			CHECK(ship.hireCaptain("Jack Sparrow"));
			CHECK(ship.hireOfficer("James Cook"));
			CHECK_FALSE(ship.setSail());
			CHECK(ship.hireCrew("Crewman 1"));
			CHECK(ship.hireCrew("Crewman 2"));
			CHECK_FALSE(ship.setSail());
		}

		SECTION("Have enough crew, captain and officer")
		{
			CHECK(ship.hireCaptain("Jack Sparrow"));
			CHECK(ship.hireOfficer("James Cook"));
			CHECK(ship.hireCrew("Crewman 1"));
			CHECK(ship.hireCrew("Crewman 2"));
			REQUIRE(ship.hireCrew("Crewman 3"));
			CHECK(ship.setSail());
		}
	}

	SECTION("There is minimal cargo value")
	{
		Ship ship(3, 5, cargoArea, 5.0);

		CHECK(ship.hireCaptain("Jack Sparrow"));
		CHECK(ship.hireOfficer("James Cook"));
		CHECK(ship.hireCrew("Crewman 1"));
		CHECK(ship.hireCrew("Crewman 2"));
		CHECK(ship.hireCrew("Crewman 3"));

		SECTION("Haven't enough value"){
			CHECK(ship.addContainer(new cargo::Container(1.0, 1.0, 1.0)));
			REQUIRE_FALSE(ship.setSail());
		}

		SECTION("Have enough value"){
			CHECK(ship.addContainer(new cargo::Container(1.0, 1.0, 10.0)));
			REQUIRE(ship.setSail());
		}
	}
}

TEST_CASE("Set sail with mocked cargo area", "[ship]")
{
	auto* cargoArea = new MockCargoArea;
	Ship ship(3, 5, cargoArea, 5.0);

	CHECK(ship.hireCaptain("Jack Sparrow"));
	CHECK(ship.hireOfficer("James Cook"));
	CHECK(ship.hireCrew("Crewman 1"));
	CHECK(ship.hireCrew("Crewman 2"));
	CHECK(ship.hireCrew("Crewman 3"));

	SECTION("Haven't enough value")
	{
		cargoArea->setCurrentValue(4.0);
		REQUIRE_FALSE(ship.setSail());
	}

	SECTION("Exactly enough value")
	{
		cargoArea->setCurrentValue(5.0);
		REQUIRE(ship.setSail());
	}

	SECTION("More then enough value")
	{
		cargoArea->setCurrentValue(6.0);
		REQUIRE(ship.setSail());
	}
}
