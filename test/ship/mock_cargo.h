#pragma once

#include <cargo-area.h>
#include <cargo-container.h>

class MockCargoArea: public cargo::CargoArea {
	public:
		MockCargoArea(): cargo::CargoArea(10.0, 10.0) {}

		[[nodiscard]] float getValue() const override
		{
			return currentValue;
		}
		[[nodiscard]] float getWeight() const override
		{
			return currentWeight;
		}
		[[nodiscard]] float getTEU() const override
		{
			return currentTEU;
		}

		void setCurrentValue(float currentValue)
		{
			this->currentValue = currentValue;
		}
		void setCurrentWeight(float currentWeight)
		{
			this->currentWeight = currentWeight;
		}
		void setCurrentTEU(float currentTEU)
		{
			this->currentTEU = currentTEU;
		}

	protected:
		float currentValue{0.0};
		float currentWeight{0.0};
		float currentTEU{0.0};
};
