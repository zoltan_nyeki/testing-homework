#include <catch.hpp>
#include <cargo-area.h>

TEST_CASE("Cargo Area", "[cargo]")
{
	cargo::CargoArea area(10.0, 10.0);
	SECTION("getTEU")
	{
		CHECK(area.addContainer(new cargo::Container(1.4, 1.4, 1.4)));
		REQUIRE(area.getTEU() == Approx(1.4));
	}
}
