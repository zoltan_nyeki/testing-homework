add_subdirectory(cargo)

add_library(ship
	ship.cpp
)

target_include_directories(ship
	PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}
)

target_link_libraries(ship
	PUBLIC Qt5::Core
	PUBLIC cargo
)
