#include "ship.h"

#include <stdexcept>

Ship::Ship(int minCrew, int maxCrew, cargo::CargoArea* cargoArea, float minValue)
	: minCrew(minCrew), maxCrew(maxCrew), cargoArea(cargoArea), minValue(minValue)
{
	if (minCrew < 0) {
		throw std::invalid_argument("minCrew is negative");
	}
	if (maxCrew < 0) {
		throw std::invalid_argument("maxCrew is negative");
	}
	if (minValue < 0.0) {
		throw std::invalid_argument("minValue is negative");
	}
	if (maxCrew < minCrew) {
		throw std::invalid_argument("maxCrew less than minCrew");
	}
}

bool Ship::hireCaptain(const QString &new_captain)
{
	if (new_captain.isEmpty() || !captain.isEmpty()) {
		return false;
	}

	captain = new_captain;

	return true;
}

bool Ship::layOffCaptain()
{
	if (captain.isEmpty()) {
		return false;
	}

	captain = "";
	return true;
}

bool Ship::hireOfficer(const QString &new_officer)
{
	if (new_officer.isEmpty() || !officer.isEmpty()) {
		return false;
	}

	officer = new_officer;

	return true;
}

bool Ship::layOffOfficer()
{
	if (officer.isEmpty()) {
		return false;
	}

	officer = "";
	return true;
}

bool Ship::hireCrew(const QString& crewman)
{
	if(crewman.isEmpty()) {
		return false;
	}

	if(crew.contains(crewman)) {
		return false;
	}

	if (crew.size() >= maxCrew) {
		return false;
	}

	crew.insert(crewman);

	return true;
}

bool Ship::layOffCrew(const QString &crewman)
{
	if(crewman.isEmpty()) {
		return false;
	}
	if(!crew.contains(crewman)) {
		return false;
	}

	crew.remove(crewman);
	return true;
}

bool Ship::setSail() const
{
	if (captain.isEmpty()) {
		return false;
	}

	if (officer.isEmpty()) {
		return false;
	}

	if (crew.size() < minCrew) {
		return false;
	}

	if (crew.size() > maxCrew) {
		return false;
	}

	if (minValue >= 0.0 && cargoArea->getValue() < minValue) {
		return false;
	}

	return true;
}

QString Ship::getCrew(bool full) const
{
	QString ret;

	if (full)
	{
		ret += "Captain: " + captain + "\n";
		ret += "Officer: " + officer  + "\n";
	}
	QStringList crewlist = crew.values();
	crewlist.sort();
	ret += "Crew: " + crewlist.join(", ");

	return ret;
}

bool Ship::addContainer(cargo::Container* container)
{
	bool retValue = false;
	if ( container == nullptr || cargoArea == nullptr) 	{
		return false;
	}

	retValue = cargoArea->addContainer(container);
	return retValue;

}
