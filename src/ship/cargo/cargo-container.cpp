#include "cargo-container.h"

namespace cargo {

Container::Container(float TEU, float weight, float value)
	: TEU(TEU), weight(weight), value(value)
{
}

float Container::getTEU() const
{
	return TEU;
}

float Container::getWeight() const
{
	return weight;
}

float Container::getValue() const
{
	return value;
}

} // namespace cargo
