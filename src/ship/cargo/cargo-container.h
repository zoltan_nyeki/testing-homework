#pragma once

namespace cargo {

class Container {
	public:
		Container(float weight, float TEU, float value);

		[[nodiscard]] float getTEU() const;
		[[nodiscard]] float getWeight() const;
		[[nodiscard]] float getValue() const;

	private:
		float TEU;
		float weight;
		float value;
};

} // namespace cargo
