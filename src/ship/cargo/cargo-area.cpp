#include "cargo-area.h"

#include <numeric>

namespace cargo {

CargoArea::CargoArea(float maximalWeight, float maximumTEU)
	: maximalWeight(maximalWeight), maximalTEU(maximumTEU)
{
}

bool CargoArea::addContainer(Container* container)
{
	if((container->getTEU() <= 0.0) || (container->getValue()<= 0.0) || (container->getWeight() <= 0.0))
	{
		return false;
	}

	if (container->getTEU() + getTEU() > this->maximalTEU)
	{
		return false;
	}
	if (container->getWeight() + getWeight() > this->maximalWeight)
	{
		return false;
	}

	containers.push_back(container);
	return true;
}

float CargoArea::getValue() const
{
	float value = std::accumulate(containers.begin(), containers.end(), 0, [](float acc, const Container* container) {
		return acc + container->getValue();
	});

	return value;
}

float CargoArea::getWeight() const
{
	float weight = std::accumulate(containers.begin(), containers.end(), 0.0f, [](float acc, const Container* container) {
		return acc + container->getWeight();
	});

	return weight;
}

float CargoArea::getTEU() const
{
	float TEU = std::accumulate(containers.begin(), containers.end(), 0.0f, [](float acc, const Container* container) {
		return acc + container->getTEU();
	});

	return TEU;
}

float CargoArea::getMaximalWeight() const
{
	return maximalWeight;
}

float CargoArea::getMaximalTEU() const
{
	return maximalTEU;
}

} // namespace cargo
