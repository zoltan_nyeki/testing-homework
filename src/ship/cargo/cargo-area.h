#pragma once

#include <QVector>

#include "cargo-container.h"

namespace cargo {

class CargoArea {
	public:
		CargoArea(float maximalWeight, float maximumTEU);

		bool addContainer(Container* container);

		[[nodiscard]] float getMaximalWeight() const;
		[[nodiscard]] float getMaximalTEU() const;

		[[nodiscard]] virtual float getValue() const;
		[[nodiscard]] virtual float getWeight() const;
		[[nodiscard]] virtual float getTEU() const;

	private:
		float maximalWeight;
		float maximalTEU;

		QVector<Container*> containers;
};

} // namespace cargo
