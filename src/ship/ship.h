#pragma once

#include <QSet>
#include <QString>

#include <cargo-area.h>
#include <cargo-container.h>

class Ship {
	public:
		Ship(int minCrew, int maxCrew, cargo::CargoArea* cargoArea = nullptr, float minValue = 0.0);

		bool hireCaptain(const QString &captain);
		bool layOffCaptain();

		bool hireOfficer(const QString &officer);
		bool layOffOfficer();

		bool hireCrew(const QString& crewman);
		bool layOffCrew(const QString& crewman);

		QString getCrew(bool full = false) const;

		bool addContainer(cargo::Container* container);

		bool setSail() const;

	protected:
		int minCrew;
		int maxCrew;
		float minValue;
		QSet<QString> crew;

		QString captain;
		QString officer;

		cargo::CargoArea* cargoArea;
};
