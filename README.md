# Specification

### Functionality
* Ship
	* can hire and lay off captain/officer/crewman
	* set sail
	* add container to the cargo
	* get crew list
	* get full crew list (included with captain and officer)

* Cargo
	* add container
	* get current value of cargo
	* get maximal weight of cargo
	* get current weight of cargo
	* get maximal TEU of cargo
	* get current TEU of cargo 
---

### Requirements

* Ship:
	* hireCaptain:
		* only hire captain with name
		* a captain can only be hired if another caption is not hired
	* lay off Captain
		* only able to lay off captain that if there
	* hire Officer
		* only hire officer with name
		* an officer can only be hired if another officer is not hired
	* it is only possible to lay off the officer that if:
		* has a name
		* is hired
	* hire crew
		* only hire crewman with name
		* crewman may be hire only until capacity of crew is reached
	* it is only possible to lay off on a crewman that if:
		* has a name
		* is hired
	* it is only possible to set sail if:
		* have enough crew
		* don't have too much crew
		* have enough valuable container if ship has cargo-area
	* get crew list
	* get all crew list (included with captain and officer)
	* add container to cargo
		* only if is there cargo
* Cargo
	* add container to cargo
		* cannot add the container with non-positive value
		* cannot add the container with non-positive TEU
		* cannot add the container with non-positive weight
		* cannot add the container if cargo will reach the maximal TEU with it
		* cannot add the container if cargo will reach the maximal weight with it
* weight, TEU and value are positive floating point number

---
*Note*

* TEU: twenty-foot equivalent unit, a unit of volume

---
